package netker.api.advertisements.controllers

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import netker.api.advertisements.models.Advertisement
import netker.api.advertisements.repository.AdvertisementRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*

@RestController
@Api(value = "advertisement", description = "Basic operations for advertisements")
class AdvertisementController {
    @Autowired
    lateinit var advertisementRepository: AdvertisementRepository

    @ApiOperation(value = "Lists all advertisements")
    @GetMapping(value = "/v1/ads", produces = arrayOf(MediaType.APPLICATION_JSON_UTF8_VALUE))
    fun listAll() = advertisementRepository.findAll()

    @ApiOperation(value = "Creates an advertisement")
    @PostMapping(value = "/v1/ads", produces = arrayOf(MediaType.APPLICATION_JSON_UTF8_VALUE))
    fun store(@RequestBody(required = true) advertisement: Advertisement): Advertisement? = advertisementRepository.save(advertisement)

    @ApiOperation(value = "Retrieves an advertisement with the given ID")
    @GetMapping(value = "/v1/ad/{id}", produces = arrayOf(MediaType.APPLICATION_JSON_UTF8_VALUE))
    fun getById(@PathVariable id: String): Advertisement = advertisementRepository.getById(id)

    @ApiOperation(value = "Updates an advertisement with the given ID")
    @PutMapping(value = "/v1/ad/{id}", produces = arrayOf(MediaType.APPLICATION_JSON_UTF8_VALUE))
    fun update(@PathVariable id: String, @RequestBody(required = true) updatedAdvertisement: Advertisement) {
        val currentAdvertisement = advertisementRepository.getById(id)
        advertisementRepository.save(updatedAdvertisement.copy(id = currentAdvertisement.id, createdAt = currentAdvertisement.createdAt))
    }

    @ApiOperation(value = "Deletes an advertisement with the given ID")
    @DeleteMapping(value = "/v1/ad/{id}", produces = arrayOf(MediaType.APPLICATION_JSON_UTF8_VALUE))
    fun removeById(@PathVariable id: String) = advertisementRepository.removeById(id)
}

