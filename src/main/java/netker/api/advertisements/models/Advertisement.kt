package netker.api.advertisements.models

import netker.api.categories.models.Category
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.PersistenceConstructor
import org.springframework.data.mongodb.core.mapping.Document
import java.time.Instant
import java.time.LocalDateTime
import java.util.*

@Document(collection = "advertisements")
data class Advertisement @PersistenceConstructor constructor(
        @Id
        val id: String = UUID.randomUUID().toString() + Instant.now().epochSecond.toString(),
        var title: String = "",
        var description: String = "",
        var category: Category? = null,
        var price: Double = 0.0,
        val createdAt: LocalDateTime = LocalDateTime.now()
)