package netker.api.advertisements.repository

import netker.api.advertisements.models.Advertisement
import netker.api.categories.models.Category
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface AdvertisementRepository : MongoRepository<Advertisement, String> {
    fun getById(id: String): Advertisement

    override fun findAll(): List<Advertisement>

    fun getByCategory(category: Category): List<Advertisement>

    fun getByTitleOrDescription(title: String, description: String): List<Advertisement>

    fun removeById(id: String)
}
