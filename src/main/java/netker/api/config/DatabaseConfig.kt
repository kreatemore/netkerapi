package netker.api.config

import com.mongodb.Mongo
import com.mongodb.MongoClient
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource
import org.springframework.data.mongodb.config.AbstractMongoConfiguration
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories

@Configuration
@EnableMongoRepositories(basePackages = arrayOf("netker.api"))
@PropertySource(value = *arrayOf("classpath:application.properties"))
open class DatabaseConfig : AbstractMongoConfiguration() {
    @Value("\${database.host}")
    private lateinit var host: String

    @Value("\${database.name}")
    private lateinit var database: String

    @Value("\${database.port}")
    private val port: Int? = null

    override fun getDatabaseName(): String = database

    @Bean
    @Throws(Exception::class)
    override fun mongo(): Mongo = MongoClient(host, port!!)

}
