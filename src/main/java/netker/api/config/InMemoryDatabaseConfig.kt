package netker.api.config

import com.github.fakemongo.Fongo
import com.mongodb.Mongo
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.data.mongodb.config.AbstractMongoConfiguration

@Profile("test")
@Configuration
open class InMemoryDatabaseConfig : AbstractMongoConfiguration() {
    override fun getDatabaseName(): String = "inMemoryDatabase"

    @Bean
    @Throws(Exception::class)
    override fun mongo(): Mongo = Fongo(databaseName).mongo

}
