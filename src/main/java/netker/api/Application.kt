package netker.api

import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.google.common.base.Predicates
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder
import springfox.documentation.RequestHandler
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.ApiInfo
import springfox.documentation.service.Contact
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2

@SpringBootApplication
open class Application {

    @Bean
    open fun objectMapperBuilder(): Jackson2ObjectMapperBuilder
            = Jackson2ObjectMapperBuilder().modulesToInstall(KotlinModule())

    @Configuration
    @EnableSwagger2
    open class SwaggerConfig {
        @Bean
        open fun api(): Docket {
            return Docket(DocumentationType.SWAGGER_2)
                    .select()
                    .apis(Predicates.not<RequestHandler>(RequestHandlerSelectors.basePackage("org.springframework.boot")))
                    .paths(PathSelectors.any())
                    .build()
                    .apiInfo(apiInfo())
        }

        open fun apiInfo(): ApiInfo {
            val contact = Contact("Alex Szabó", "http://www.tailored.hu", "kreatemore@gmail.com")
            return ApiInfo(
                    "Netker Public API",
                    "Basic overview of the Netker REST API.",
                    "v1.0.0",
                    "TBA",
                    contact,
                    "TBD",
                    "http://www.google.com")
        }
    }
}

fun main(args: Array<String>) {
    SpringApplication.run(Application::class.java, *args)
}