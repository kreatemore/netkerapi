package netker.api.healthcheck.models

import org.springframework.data.annotation.Id
import org.springframework.data.annotation.PersistenceConstructor
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "healthcheck")
data class ApiHealthcheck @JvmOverloads constructor(
        @Id
        private val id: String? = null,
        val message: String? = null
)

