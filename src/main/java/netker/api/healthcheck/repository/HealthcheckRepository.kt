package netker.api.healthcheck.repository

import netker.api.healthcheck.models.ApiHealthcheck
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface HealthcheckRepository : MongoRepository<ApiHealthcheck, String> {
    override fun findAll(): List<ApiHealthcheck>

    fun removeByMessage(message: String)
}
