package netker.api.healthcheck.controllers

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import netker.api.healthcheck.models.ApiHealthcheck
import netker.api.healthcheck.repository.HealthcheckRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

import org.springframework.web.bind.annotation.RequestMethod.GET

@RestController
@Api(value = "healthcheck", description = "A simple healthcheck endpoint")
class HealthcheckController {
    @Autowired
    private val healthcheckRepository: HealthcheckRepository? = null

    @ApiOperation(value = "You can use this to determine if Netker API is available")
    @RequestMapping(value = "/v1/healthcheck", method = arrayOf(GET), produces = arrayOf(MediaType.APPLICATION_JSON_UTF8_VALUE))
    fun healthCheck(@RequestParam(value = "name", defaultValue = "World") name: String): ApiHealthcheck = ApiHealthcheck(message = "Hello ${name}!".format(name))

    @ApiOperation(value = "This can be used to determine if the underlying MongoDB can be accessed")
    @RequestMapping(value = "/v1/healthcheck/database", method = arrayOf(GET), produces = arrayOf(MediaType.APPLICATION_JSON_UTF8_VALUE))
    fun databaseHealthcheck(): List<ApiHealthcheck> = healthcheckRepository!!.findAll()
}
