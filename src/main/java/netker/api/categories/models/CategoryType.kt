package netker.api.categories.models

enum class CategoryType {
    MAIN,
    SUB
}
