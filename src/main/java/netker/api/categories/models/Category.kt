package netker.api.categories.models

import org.springframework.data.annotation.Id
import org.springframework.data.annotation.PersistenceConstructor
import org.springframework.data.mongodb.core.mapping.Document
import java.time.Instant

import java.util.Optional
import java.util.UUID

@Document(collection = "categories")
data class Category @PersistenceConstructor constructor(
        @Id
        val id: String = UUID.randomUUID().toString() + Instant.now().epochSecond.toString(),
        var categoryType: CategoryType? = null,
        var name: String? = null,
        var parent: Optional<Category>? = null
)