package netker.api.categories.controllers

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import netker.api.categories.models.Category
import netker.api.categories.repository.CategoryRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

import org.springframework.web.bind.annotation.RequestMethod.*

@RestController
@Api(value = "category", description = "Basic operations for categories")
class CategoryController {
    @Autowired
    private lateinit var categoryRepository: CategoryRepository

    @ApiOperation(value = "Lists all categories")
    @RequestMapping(value = "/v1/categories", method = arrayOf(GET), produces = arrayOf(MediaType.APPLICATION_JSON_UTF8_VALUE))
    fun listAll(): List<Category> = categoryRepository.findAll()

    @ApiOperation(value = "Creates a category")
    @RequestMapping(value = "/v1/categories", method = arrayOf(POST), produces = arrayOf(MediaType.APPLICATION_JSON_UTF8_VALUE))
    fun store(@RequestBody(required = true) category: Category) = categoryRepository.save(category)

    @ApiOperation(value = "Retrieves a category with the given ID")
    @RequestMapping(value = "/v1/category/{id}", method = arrayOf(GET), produces = arrayOf(MediaType.APPLICATION_JSON_UTF8_VALUE))
    fun getById(@PathVariable id: String): Category = categoryRepository.getById(id)

    @ApiOperation(value = "Updates a category with the given ID")
    @RequestMapping(value = "/v1/category/{id}", method = arrayOf(PUT), produces = arrayOf(MediaType.APPLICATION_JSON_UTF8_VALUE))
    fun update(@PathVariable id: String, @RequestBody(required = true) category: Category) {
        val currentCategory = categoryRepository.getById(id)
        categoryRepository.save(category.copy(id = currentCategory.id))
    }

    @ApiOperation(value = "Deletes a category with the given ID")
    @RequestMapping(value = "/v1/category/{id}", method = arrayOf(DELETE), produces = arrayOf(MediaType.APPLICATION_JSON_UTF8_VALUE))
    fun removeById(@PathVariable id: String) = categoryRepository.removeById(id)
}

