package netker.api.categories.repository

import netker.api.categories.models.Category
import netker.api.categories.models.CategoryType
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository
import java.util.Optional

@Repository
interface CategoryRepository : MongoRepository<Category, String> {
    fun getById(id: String): Category

    fun findByName(name: String): List<Category>

    fun findByParent(parent: Optional<Category>): List<Category>

    fun findByCategoryType(categoryType: CategoryType): List<Category>

    override fun findAll(): List<Category>

    fun removeById(id: String)
}
