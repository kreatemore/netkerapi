package netker.api.healthcheck

import netker.api.healthcheck.models.ApiHealthcheck
import netker.api.healthcheck.repository.HealthcheckRepository
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

import org.hamcrest.Matchers.`is`


@ActiveProfiles("test")
@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class HealthcheckControllerTest {
    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var healthcheckRepository: HealthcheckRepository

    @Test
    @Throws(Exception::class)
    fun healthcheckCanBeReachedAndIsReturningOK() {
        mockMvc.perform(MockMvcRequestBuilders.get("/v1/healthcheck"))
                .andExpect(MockMvcResultMatchers.status().isOk)

    }

    @Test
    @Throws(Exception::class)
    fun healthcheckCanBeReachedAndIsReturningDefaultMessage() {
        mockMvc.perform(MockMvcRequestBuilders.get("/v1/healthcheck"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.jsonPath("$.message", `is`("Hello World!")))

    }

    @Test
    @Throws(Exception::class)
    fun healthcheckReturnsMessageFromInputParam() {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/v1/healthcheck")
                .param("name", "Service"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.jsonPath("$.message", `is`("Hello Service!")))
    }

    @Test
    @Throws(Exception::class)
    fun databaseHealthcheckCanBeAccessed() {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/v1/healthcheck/database"))
                .andExpect(MockMvcResultMatchers.status().isOk)
    }

    @Test
    @Throws(Exception::class)
    fun databaseHealthcheckListsEntries() {
        val message = "Database"
        val apiHealthcheck = ApiHealthcheck(message = message)

        // Clean if anything in there
        healthcheckRepository.removeByMessage(message)

        // Store a value, and expect that in return
        healthcheckRepository.save(apiHealthcheck)
        mockMvc.perform(MockMvcRequestBuilders
                .get("/v1/healthcheck/database"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.content().json("[{'message':'Database'}]"))

        // Clear everything, expect an empty array in return
        healthcheckRepository.removeByMessage(message)
        mockMvc.perform(MockMvcRequestBuilders
                .get("/v1/healthcheck/database"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.content().json("[]"))
    }

}