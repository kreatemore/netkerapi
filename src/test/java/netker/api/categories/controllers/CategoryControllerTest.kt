package netker.api.categories.controllers

import com.fasterxml.jackson.databind.ObjectMapper
import netker.api.categories.models.Category
import netker.api.categories.models.CategoryType
import netker.api.categories.repository.CategoryRepository
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

import java.util.Optional

import org.hamcrest.Matchers.`is`


@ActiveProfiles("test")
@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class CategoryControllerTest {
    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var categoryRepository: CategoryRepository

    @Test
    @Throws(Exception::class)
    fun categoriesCanBeListed() {
        val category = createAndInsertMockCategory()
        mockMvc.perform(MockMvcRequestBuilders.get("/v1/categories"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].name", `is`("Teszt")))
        removeRecentlyCreatedCategory(category)
    }

    private fun createAndInsertMockCategory(): Category {
        val category = createMockCategory()
        categoryRepository.save(category)
        return category
    }


    @Test
    @Throws(Exception::class)
    fun categoryCanBeCreated() {
        val category = createMockCategory()
        mockMvc.perform(MockMvcRequestBuilders.post("/v1/categories")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(category)))
                .andExpect(MockMvcResultMatchers.status().isOk)

        Assert.assertEquals(1, categoryRepository.findAll().size.toLong())
        Assert.assertEquals(category.id, categoryRepository.findAll()[0].id)
        removeRecentlyCreatedCategory(category)
    }

    @Test
    @Throws(Exception::class)
    fun categoryCanBeRetrievedById() {
        val category = createAndInsertMockCategory()
        mockMvc.perform(MockMvcRequestBuilders.get("/v1/category/" + category.id))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", `is`("Teszt")))
        removeRecentlyCreatedCategory(category)
    }

    @Test
    @Throws(Exception::class)
    fun categoryCanBeDeletedById() {
        val (id) = createAndInsertMockCategory()
        mockMvc.perform(MockMvcRequestBuilders.delete("/v1/category/" + id))
                .andExpect(MockMvcResultMatchers.status().isOk)

        Assert.assertEquals(0, categoryRepository.findAll().size.toLong())
    }

    @Test
    @Throws(Exception::class)
    fun categoryCanBeUpdated() {
        val category = createAndInsertMockCategory()

        val updatedCategory = createMockCategory()
        updatedCategory.name = "Updated"

        mockMvc.perform(MockMvcRequestBuilders.put("/v1/category/" + category.id)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(updatedCategory)))
                .andExpect(MockMvcResultMatchers.status().isOk)

        Assert.assertEquals(1, categoryRepository.findAll().size.toLong())
        Assert.assertEquals(category.id, categoryRepository.findAll()[0].id)
        Assert.assertEquals(updatedCategory.name, categoryRepository.findAll()[0].name)
        removeRecentlyCreatedCategory(category)
    }

    private fun removeRecentlyCreatedCategory(category: Category) {
        categoryRepository.removeById(category.id)
    }

    private fun createMockCategory(): Category {
        return Category(categoryType = CategoryType.MAIN, name = "Teszt")
    }

}