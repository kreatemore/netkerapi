package netker.api.advertisement.controllers

import com.fasterxml.jackson.databind.ObjectMapper
import netker.api.advertisements.models.Advertisement
import netker.api.advertisements.repository.AdvertisementRepository
import netker.api.categories.models.Category
import netker.api.categories.models.CategoryType
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

import java.util.Optional

import org.hamcrest.Matchers.`is`


@ActiveProfiles("test")
@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class AdvertisementControllerTest {
    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var advertisementRepository: AdvertisementRepository
    private val category = createMockCategory()


    @Test
    @Throws(Exception::class)
    fun advertisementsCanBeListed() {
        advertisementRepository.save(createMockAdvertisement(category))

        mockMvc.perform(MockMvcRequestBuilders.get("/v1/ads"))
                .andExpect(MockMvcResultMatchers.status().isOk)

        clearRecentlyCreatedAdvertisement()
    }

    @Test
    @Throws(Exception::class)
    fun advertisementCanBeRetrievedById() {
        advertisementRepository.save(createMockAdvertisement(category))
        val id = advertisementRepository.findAll()[0].id

        mockMvc.perform(MockMvcRequestBuilders.get("/v1/ad/" + id))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.jsonPath("$.title", `is`("Title")))

        clearRecentlyCreatedAdvertisement()
    }

    @Test
    @Throws(Exception::class)
    fun advertisementCanBeCreated() {
        val advertisement = createMockAdvertisement(category)

        mockMvc.perform(MockMvcRequestBuilders.post("/v1/ads")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(advertisement)))
                .andExpect(MockMvcResultMatchers.status().isOk)

        mockMvc.perform(MockMvcRequestBuilders.get("/v1/ads"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].title", `is`("Title")))

        clearRecentlyCreatedAdvertisement()
    }

    @Test
    @Throws(Exception::class)
    fun advertisementCanBeDeletedById() {
        advertisementRepository.save(createMockAdvertisement(category))
        val id = advertisementRepository.findAll()[0].id

        mockMvc.perform(MockMvcRequestBuilders.delete("/v1/ad/" + id))
                .andExpect(MockMvcResultMatchers.status().isOk)

        Assert.assertEquals(0, advertisementRepository.findAll().size.toLong())
    }

    @Test
    @Throws(Exception::class)
    fun advertisementCanBeUpdated() {
        val advertisement = createMockAdvertisement(category)
        advertisementRepository.save(advertisement)
        val id = advertisementRepository.findAll()[0].id

        val updatedAdvertisement = createMockAdvertisement(category)
        updatedAdvertisement.title = "Updated"

        mockMvc.perform(MockMvcRequestBuilders.put("/v1/ad/" + id)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(updatedAdvertisement)))
                .andExpect(MockMvcResultMatchers.status().isOk)

        Assert.assertEquals(1, advertisementRepository.findAll().size.toLong())
        Assert.assertEquals(advertisement.id, advertisementRepository.findAll()[0].id)
        Assert.assertEquals(updatedAdvertisement.title, advertisementRepository.findAll()[0].title)

        clearRecentlyCreatedAdvertisement()
    }

    private fun createMockAdvertisement(category: Category): Advertisement {
        return Advertisement(title = "Title", category = category, description = "Test", price = 5000.0, id = "1")
    }

    private fun createMockCategory(): Category {
        return Category(categoryType = CategoryType.MAIN, name = "Category")
    }


    private fun clearRecentlyCreatedAdvertisement() {
        val id = advertisementRepository.findAll()[0].id
        advertisementRepository.removeById(id)
    }


}