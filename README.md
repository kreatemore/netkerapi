# README #

Netker API

### What is this repository for? ###

Netker API is written in Kotlin with Spring, and uses MongoDB as a database.

Purpose is not yet clarified - this will be a GraphQL backend for most likely a classified/listing app.

Frontend will be a separate project, with Angular + Redux.

### How do I get set up? ###

* Maven has all the dependencies
* Apart from this, MongoDB will need to be installed locally.